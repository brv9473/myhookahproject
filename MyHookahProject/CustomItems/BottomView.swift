//
//  BottomView.swift
//  Animations
//
//  Created by Rostyslav Bodnar on 8/1/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class BottomView: UIView {
    
    let toolBar = UIView()
    var picker = UIPickerView()
    var barButton = UIButton()
    var barButtonAdd = UIButton()
    


    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        setup()
    }
    
    
    func setup()  {
        
        barButton.setTitle("Відмінити", for: .normal)
        barButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        
        barButtonAdd.setTitle("Добавити", for: .normal)
        
        toolBar.backgroundColor = .lightGray
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(toolBar)
        toolBar.layer.shadowColor = UIColor.black.cgColor
        toolBar.layer.shadowOpacity = 1
        toolBar.layer.shadowOffset = .zero
        toolBar.layer.shadowRadius = 3
        
        
        self.backgroundColor = .white
        self.translatesAutoresizingMaskIntoConstraints = false
        
        
        picker = UIPickerView.init()
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(picker)

        barButton.backgroundColor = .lightGray
        barButton.translatesAutoresizingMaskIntoConstraints = false
        toolBar.addSubview(barButton)
        
        barButtonAdd.backgroundColor = .lightGray
        barButtonAdd.translatesAutoresizingMaskIntoConstraints = false
        toolBar.addSubview(barButtonAdd)
        
        NSLayoutConstraint.activate([
  
            barButton.leadingAnchor.constraint(equalTo: toolBar.leadingAnchor, constant: 10),
            barButton.topAnchor.constraint(equalTo: toolBar.topAnchor, constant:  10),
            barButton.bottomAnchor.constraint(equalTo: toolBar.bottomAnchor, constant: -10),
            
            barButtonAdd.rightAnchor.constraint(equalTo: toolBar.rightAnchor, constant: -10),
            barButtonAdd.topAnchor.constraint(equalTo: toolBar.topAnchor, constant: 10),
            barButtonAdd.bottomAnchor.constraint(equalTo: toolBar.bottomAnchor, constant: -10),
            
            toolBar.topAnchor.constraint(equalTo: self.topAnchor),
            toolBar.leftAnchor.constraint(equalTo: self.leftAnchor),
            toolBar.widthAnchor.constraint(equalTo: self.widthAnchor),
            toolBar.heightAnchor.constraint(equalToConstant: 35),
            
            picker.leftAnchor.constraint(equalTo: self.leftAnchor),
            picker.rightAnchor.constraint(equalTo: self.rightAnchor),
            picker.topAnchor.constraint(equalTo: toolBar.bottomAnchor),
            picker.bottomAnchor.constraint(equalTo: self.bottomAnchor),

            ])
    }
    
    var cnstrBottom: NSLayoutConstraint?
    var cnstrTop: NSLayoutConstraint?
    
    func show(on view: UIView) {
        
        view.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        cnstrTop = self.topAnchor.constraint(equalTo: view.bottomAnchor)
        
        
        NSLayoutConstraint.activate([
            cnstrTop!,
            self.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.rightAnchor.constraint(equalTo: view.rightAnchor),
            ])
        
        view.layoutIfNeeded()
        
        cnstrTop!.isActive = false
        
        cnstrBottom = self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        cnstrBottom?.isActive = true
        
        UIView.animate(withDuration: 0.3) {
            view.layoutIfNeeded()
        }
        
    }
    
    @objc func close() {
        
        if self.superview == nil {
            return
        }
        
        cnstrBottom?.isActive = false
        cnstrTop!.isActive = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.superview?.layoutIfNeeded()
        
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
}
