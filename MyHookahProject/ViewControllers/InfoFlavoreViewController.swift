//
//  InfoFlavoreViewController.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/11/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class InfoFlavoreViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{


    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    var bottonView = BottomView()
    var flavore: Flavore!
    let arrayComponents = ["5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80", "85", "90", "95", "100"]
    var percent: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = flavore.id
        
        //MARK: - Image
        imageView.image = UIImage(named: flavore.id)
        
        //MARK: - Info flavore label
        infoLabel.lineBreakMode = .byWordWrapping
        infoLabel.numberOfLines = 0
        infoLabel.text = flavore.info
        
        
        //MARK: - Add picker
        bottonView.picker.dataSource = self
        bottonView.picker.delegate = self

        
        //MARK: - AddMix Button
        
        bottonView.barButtonAdd.addTarget(self, action: #selector(addMix), for: .touchUpInside)
    }
    
  
    @IBAction func adddFlavoredButton(_ sender: Any) {
        bottonView.show(on: self.tabBarController!.view)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return DataBase.shared.availablePercentsInMix() / 5
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return arrayComponents[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        percent = Int(arrayComponents[row])
    }
    
    @objc func addMix() {
        
        
        let tabaccoId = DataBase.shared.getTabacoo(for: self.flavore)?.id
        
        DataBase.shared.addFlavorToMix(flavor: MixFlavoreds.init(percent: percent,
                                                                 tabaccoID: tabaccoId!,
                                                                 id: self.flavore.id,
                                                                 name: self.flavore.name,
                                                                 info: self.flavore.info,
                                                                 linesTabaccoIDs: []))
        bottonView.close()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        bottonView.close()
    }

}

