//
//  ViewController.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/9/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn



class ViewController: UIViewController, GIDSignInUIDelegate {
    
    //Facebook sign in button
    @IBOutlet weak var facebookButton: FBLoginButton!
    
    @IBAction func goAction(_ sender: UIButton) {
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        //Google log in button
        GIDSignIn.sharedInstance()?.uiDelegate = self
        let gLoginButton = GIDSignInButton(frame: CGRect(x: 100, y: 1000, width: 100, height: 100))
        view.addSubview(gLoginButton)        
    }

    //Highlight statusBar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
}

