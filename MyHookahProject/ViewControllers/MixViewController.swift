//
//  MixViewController.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/17/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit
import Charts


class MixViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var mix: Mix!
    let pieChartView = PieChartView()
    var a = Double()
    var b = String()
     var tabacco: Tabacco!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.view.backgroundColor = .white
        // Cell registration
        tableView.register(UINib.init(nibName: "MixTableViewCell", bundle: nil), forCellReuseIdentifier: "MixTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //MARK: pieChartView
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 370))
        customView.backgroundColor = UIColor.clear
        pieChartView.chartDescription?.enabled = false
        pieChartView.drawHoleEnabled = true
        pieChartView.rotationAngle = 0
        pieChartView.rotationEnabled = false
        pieChartView.isUserInteractionEnabled = true
        pieChartView.translatesAutoresizingMaskIntoConstraints = false
        pieChartView.legend.enabled = true
        
        pieChartView.animate(xAxisDuration: 1.3, easingOption: ChartEasingOption.easeOutBack)
        pieChartView.animate(yAxisDuration: 1.3, easingOption: ChartEasingOption.easeOutBack)
        
        var entries: [PieChartDataEntry] = Array()

        
        for i in 0..<mix.flavore.count {
            a = Double(mix.flavore[i].percent)
            b = mix.flavore[i].name
            
             entries.append(PieChartDataEntry(value: a, label: b))
        }

        let dataSet = PieChartDataSet(entries: entries, label: "")
        
        dataSet.drawValuesEnabled = true
        var colors: [UIColor] = []
        
        for _ in 0..<mix.flavore.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        dataSet.colors = colors
        pieChartView.data = PieChartData(dataSet: dataSet)
        customView.addSubview(pieChartView)
        
        NSLayoutConstraint.activate([
            pieChartView.topAnchor.constraint(equalTo: customView.topAnchor),
            customView.rightAnchor.constraint(equalTo: pieChartView.rightAnchor),
            pieChartView.leftAnchor.constraint(equalTo: customView.leftAnchor),
            pieChartView.heightAnchor.constraint(equalTo: customView.heightAnchor)
            ])
        tableView.tableFooterView = customView

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  mix.flavore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mixTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MixTableViewCell", for: indexPath) as! MixTableViewCell
        
        //MARK: - ProgressView
        mixTableViewCell.nameTabaccoLabel.text = mix.flavore[indexPath.row].tabaccoID
        mixTableViewCell.nameFlavoredLabel.text = mix.flavore[indexPath.row].name
        let percetTabacco = Float(mix.flavore[indexPath.row].percent) / 100
        mixTableViewCell.percentProgressView.setProgress(percetTabacco, animated: true)
        mixTableViewCell.percentProgressView.progressTintColor = UIColor.blue
        mixTableViewCell.percentProgressView.trackTintColor = UIColor.white

        return mixTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InfoFlavoreViewController") as! InfoFlavoreViewController
        vc.flavore = DataBase.shared.getFlavored(id: mix.flavore[indexPath.row].name)
        
        navigationController?.pushViewController(vc, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
