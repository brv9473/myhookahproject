//
//  FlavoreViewController.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/11/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class FlavoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tabacco: Tabacco!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataBase.shared.getFlavores(tabacco: tabacco).count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = DataBase.shared.getFlavores(tabacco: tabacco)[indexPath.row].name
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        title = tabacco.name
        
    }

    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InfoFlavoreViewController") as! InfoFlavoreViewController
        vc.flavore = DataBase.shared.getFlavores(tabacco: tabacco)[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
