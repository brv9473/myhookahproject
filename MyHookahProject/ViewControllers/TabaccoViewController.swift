//
//  TabaccoViewController.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/10/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class TabaccoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var country: Country!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataBase.shared.getTabaccos(for: country).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = DataBase.shared.getTabaccos(for: country)[indexPath.row].name
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()

tableView.dataSource = self
        tableView.delegate = self
        
        title = country.title
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FlavoreViewController") as! FlavoreViewController
        vc.tabacco = DataBase.shared.getTabaccos(for: country)[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

