//
//  CountrylistViewController.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/10/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class CountryListViewController: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

}

extension CountryListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataBase.shared.countrys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let countryModel = DataBase.shared.countrys[indexPath.row]
        
        cell.textLabel?.text = countryModel.title
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TabaccoViewController") as! TabaccoViewController
            vc.country = DataBase.shared.countrys[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
