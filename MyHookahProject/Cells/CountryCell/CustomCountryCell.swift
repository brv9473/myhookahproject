//
//  CustomCountryCell.swift
//  MyHookahProject
//
//  Created by Rostyslav Bodnar on 10/2/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class CustomCountryCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var imageFlagCountry: UIImageView!
    @IBOutlet weak var labelCountry: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        contentView.backgroundColor = .clear
        
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = 0.4
        view.layer.shadowRadius = 8
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.masksToBounds = false
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
