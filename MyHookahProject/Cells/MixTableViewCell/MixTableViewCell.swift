
//
//  MixTableViewCell.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/18/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import UIKit

class MixTableViewCell: UITableViewCell {

    @IBOutlet weak var nameTabaccoLabel: UILabel!
    @IBOutlet weak var nameFlavoredLabel: UILabel!
    @IBOutlet weak var percentProgressView: UIProgressView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
