//
//  DataBase.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/10/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation
import FirebaseFirestore
import CodableFirebase


class DataBase {
    let countrys: [Country]
    let tabaccos: [Tabacco]
    let flavores: [Flavore]
    let lines: [LineTabacco]
    var mixes: [Mix] = []
    let db =  Firestore.firestore()
    
    static let shared = DataBase()
    


    init() {
        
        self.countrys = [Country(id: "Russia", title: "Russia"),
                    
                    Country(id: "USA", title: "USA"),
                    
                    Country(id: "Germany", title: "Germany"),
                    
                    Country(id: "Turkey", title: "Turkey"),
                    
                    Country(id: "United Arab Emirates", title: "United Arab Emirates"),
                    
                    Country(id: "India", title: "India"),
                    
                    Country(id: "Egypt", title: "Egypt")]
        

        tabaccos =  [Tabacco.init(id: "Dark Side", name: "Dark Side", countryID: "Russia"),
                                  Tabacco.init(id: "Daily Hookah", name: "Daily Hookah", countryID: "Russia"),
                                  Tabacco.init(id: "D – Mini", name: "D – Mini", countryID: "Russia"),
                                  Tabacco.init(id: "D – Gastro", name: "D – Gastro", countryID: "Russia"),
                                  Tabacco.init(id: "Tangiers", name: "Tangiers", countryID: "USA"),
                                  Tabacco.init(id: "Fumari", name: "Fumari", countryID: "USA"),
                                  Tabacco.init(id: "Cult", name: "Cult", countryID: "Germany"),
                                  Tabacco.init(id: "Serbetli", name: "Serbetli", countryID: "Turkey"),
                                  Tabacco.init(id: "Al Fakher", name: "Al Fakher", countryID: "United Arab Emirates"),
                                  Tabacco.init(id: "Afzal", name: "Afzal", countryID: "India"),
                                  Tabacco.init(id: "Nakhla", name: "Nakhla", countryID: "Egypt")]



        flavores = [Flavore.init(id: "Eclipse", name: "Eclipse", info: "DarkSide Eclipse (Медовые леденцы с цитрусом) – мягкое, обволакивающее и освежающее сочетание сладких медовых леденцов и ароматного мандарина. Густой дым согревает и расслабляет, полностью передавая гармоничный и теплый вкус.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Code Cherry", name: "Code Cherry", info: "DarkSide Code Cherry (Вишневый код) – смесь отличается идеальной вкусопередачей и безупречным купажом, благодаря которому аромат спелой вишни заиграл новыми красками. Code Cherry передает не только богатый кисло-сладкий букет спелой вишни, но также оттенки вишневого сока, леденца и варенья. В финале ощущается терпкое послевкусие вишневой косточки. Code Cherry отлично воспринимается соло, а в миксах сочетается с большинством табаков, наилучшим образом раскрываясь в смесях с колой и чаем, лимоном и мятой, бананом, персиком и клубникой.", tabaccoID: "Dark Side")
                        ,
                    Flavore.init(id: "Tropic Ray", name: "Tropic Ray", info: "Darkside Tropic Ray — кальянный табак, который придётся по вкусу всем любителям цитрусовых и сладких тропических вкусов и ценителям исключительного качества табака Darkside. Особенно хорош такой табак для женской аудитории ввиду особенностей вкуса и высокого качества, которое вместе с средней крепостью сделает курение максимально приятной процедурой и не вызовет ухудшения самочувствия даже при длительном покуре.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Grape Core", name: "Grape Core", info: "DarkSide Grape Core (Черный виноград) – универсальный, сочный и естественный моновкус красного винограда. Умеренно сладкий, терпкий, с яркой кислинкой на выдохе. Послевкусие у Grape Core держится долго, ощущается как виноградный сок, слегка приправленный мятой. Новый вариант популярного муасселя для кальяна получил высокую оценку как экспертов, так и пользователей, в первую очередь, благодаря насыщенности и удачному купажу. Grape Core – хорошо сбалансированная и самодостаточная смесь. В миксах хорошо оттеняет мятные и цитрусовые табаки, чернично-клубничные, банановые и арбузно-дынные вариации.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "SkyLime", name: "SkyLime", info: "DarkSide SkyLime (Лайм) - прикольный и яркий, реально небесный вкус лайма с добавлением мяты, причем, понятно, почему он sky - очень легкий и воздушный что ли. Конечно, на высоте и сочность, и послевкусие, и основной букет и все остальное. Один из лучших тандемов разряда лайм-мята, куда более запоминающийся, чем большинство подобных ароматов. И все, что вам требуется - это забить его и наслаждаться невероятной цитрусовой свежестью, вкупе с божественным послевкусием.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Ice Granny", name: "Ice Granny", info: "DarkSide Ice Granny (Зеленое яблоко с ментолом) – свежий, но не обжигающий, сладкий, но не приторный вкус классического зеленого яблока Гренни Смит. При сильной концентрации напоминает леденец, на выдохе ощущается приятный и нежный холодок. Без примеси аниса. Ice Granny чудесно гармонирует с клюквой и черникой, бананом и вишней, апельсином и грушей, айвой и лаймом.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Glitch Ice Tea", name: "Glitch Ice Tea", info: "DarkSide Glitch Ice Tea (Холодный персиковый чай) – натуральный, нежный и тонкий вкус популярного напитка, который по достоинству оценят табачные гурманы. В этой сладкой и одновременно освежающей кальянной смеси вначале четко звучит персиковая нота, которую в финале сменяет приятная прохлада черного чая с легким мятным флером. В миксах Glitch Ice Tea прекрасно гармонирует с десертными и цитрусовыми табаками, а также с малиной, вишней и бергамотом.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Polar Cream", name: "Polar Cream", info: "DarkSide Polar Cream (Фисташковое мороженое) – тонкий, ласковый и волшебный вкус нежнейшего фисташкового мусса. Polar Cream стал одной из несомненных удач миксологов DarkSide, которые соединили освежающую прохладу мороженого, мяту и дразнящий аромат жареной фисташки. Этот табачный сорбет понравится всем сладкоежкам в сольном варианте, также Polar Cream образует интересные миксы в сочетании с лимоном, бананом, фундуком, ежевикой и сладкими цитрусами.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Cinnamoon", name: "Cinnamoon", info: "Табак для кальяна DarkSide Cinnamon Medium (Корица) - вкус свежей коричной булочки, при этом корица - очень легкая и приятная и совсем не ядерная. Курить очень приятно даже чистым, а для добавления в миксы, особенно в десертные - сам бог велел.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Green Beam", name: "Green Beam", info: "DarkSide Green Beam (Фейхоа) – богатый аромат экзотической зеленой ягоды, с красочной палитрой оттенков. В кисло-сладком табачном муасселе Green Beam воплощены все грани вкуса настоящего фейхоа – смеси киви, клубники, земляники, ананаса и банана. При этом наиболее полно этот букет раскрывается на выдохе. Есть в нем и травянистый компонент. Оригинальная композиция от DarkSide понравится любителям фруктовых ароматов, вкус ненавязчивый, хорошо воспринимается в моноварианте. В миксах Green Beam превосходно раскрывается вместе с другими тропическими плодами, а также с огурцом, цитрусовым чаем, грейпфрутом и малиной.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Astro Tea", name: "Astro Tea", info: "Darkside Astro Tea — это один из самых тонких ароматов у производителя. Нам нравится он по нескольким очевидным моментов. Первый из которых это безусловный вкус лимонного чая, который легко влюбляет Вас с первой затяжки. В основе вкуса находится женьшеневый вкус, который загадочным образом раскрывается только послевкусием в остальном, обычный чай с лимоном. Этот аромат, чаще других чайных, используется при покуре в соло, но мы так же придумали небольшой микс с Дарксайд Астро Ти, который довольно просто изготовить и подать.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "CucumStar", name: "CucumStar", info: "DarkSide Cucumstar (Огурец) – запоминающийся свежий аромат, рассчитан на любителей оригинальных новинок в кальянной индустрии. Вкус Cucumstar несладкий, хорошо воссоздан запах свежего огурца, но по вкусовым ощущениям напоминает солоноватый огуречный напиток. Это не самый яркий букет из фруктово-овощной линейки DarkSide, лучше раскрывается в миксах. Cucumstar гармонирует с лимоном и базиликом, фейхоа, мятой и яблоком, пряностями и травяными ароматами.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Falling Star", name: "Falling Star", info: "Darkside Rare Falling Star (Манго, Маракуйя) — Этот вкус можно просто назвать эталонным. Идеальное сочетание двух ярких представителей тропиков — манго и маракуйя. Здесь привлекательная коллаборация экзотической свежести, сладости и легкой, ненавязчивой кислинки. Высокая крепость табак подарит вам незабываемый вечер отдыха и расслабления.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Bounty Hunter", name: "Bounty Hunter", info: "DarkSide Bounty Hunter (Кокос с мятой) — охлаждающее райское наслаждение. Ванильная сладость кокоса с нежным ментолом. То, что надо, если хочется чего-нибудь нестандартного.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Space Lychee", name: "Space Lychee", info: "DarkSide Space Lychee (Космический личи) – дразнящий сладкий аромат, идеальное воплощение в табаке вкуса «китайской сливы». Аутентичность и точное попадание по купажу вкупе с легким пряным компонентом сделали Space Lychee одним из топовых ароматов во фруктовой линейке DarkSide. Многослойный и яркий вкус этого тропического плода, имеющего оттенки ананаса, винограда, клубники и бергамота, хорошо дополняет различные ягодные, цитрусовые, мятные и «алкогольные» табаки. Наиболее удачно Space Lychee сочетается с персиком, клубникой и яблоком.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Sweet Comet", name: "Sweet Comet", info: "DarkSide Sweet Comet (Клюква и банан) – необычный десертный микс для кальяна, очень сочный и душистый. В смеси первой проявляется терпкая, ароматная и кисловатая клюква, а на выдохе четко ощущается сладость и бархатистость бананового коктейля. Также присутствует оттенок морозной свежести. В сочетании с другими табаками DarkSide Sweet Comet усилит звучание ромовых, вишневых и арбузных нот, гармонирует с колой и красной смородиной, красным чаем и «овсянкой».", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Basil Blast", name: "Basil Blast", info: "DarkSide Basil Blast (Базилик) – это ароматный и очень освежающий вкус базилика. Одновременно напоминает Extragon, благодаря своему т0равянистому оттенку и Supernova из-за невероятной свежести. Выраженный вкус базилика, вместе с травянистыми и освежающими нотами образует совершенно новый вкус, который достоин внимания.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Cosmo Flower", name: "Cosmo Flower", info: "DarkSide Cosmo Flower (Цветочный вкус) — это приятный, сладковатый вкус полевых цветов. Отличный вкус для создания интересных кальянных миксов. Экспериментируйте!", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Extragon", name: "Extragon", info: "DarkSide Extragon (Тархун) —  эфирный специфический вкус тархуна не оставляет никого равнодушным. Его либо обожают, либо терпеть не могут. Этот вкус просто обязан быть в коллекции фанатов одноименного лимонада, потому что даже 5% этого табака, добавленного в микс, будет ненавязчиво доминировать и раскрываться в процессе курения.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Fruity Dust", name: "Fruity Dust", info: "DarkSide Fruity Dust (Питайя) – табак с тропическим и ярким, сладковатым вкусом питайи. Экзотичный вкус дополняется легкой кислинкой личи, а долгое (около полутора часов) время курения дает возможность в полной мере насладиться этим неординарным табаком.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Gonzo Cake", name: "Gonzo Cake", info: "DarkSide Gonzo Cake (Чизкейк) – табак с изящным и нежным вкусом сливочного пирога с малиной. Мягкая и облегающая, густая кремовая сладость дополняется легкой ягодной кислинкой малины, порождая изысканный десертный вкус.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Needls", name: "Needls", info: "DarkSide Needls (Хвоя) — это яркий, насыщенный вкус хвои. Интересная и необычная табачная смесь. Придется по вкусу любителям «пряных» кальянных миксов.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Neon Melon", name: "Neon Melon", info: "DarkSide Neonmelon - считается олдскульным вкусом арбуза. Арбузная свежесть Неонмелон является изюминкой для любителей простых дымных забивок. Аромат дыма имеет простую составляющую – это вкус сладкого, всем привычного свежего арбуза. Фруктовая ягода арбуз не боится жара, табак хорошо выдерживает большое количество углей, от чего его вкус раскрывается еще лучше. Арбузные вкусы отлично сочетаются с тропическими фруктами и дыней.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Space Dessert", name: "Space Dessert", info: "DarkSide Space Dessert (Тирамису) – это воздушный вкус такого популярного десерта Тирамису. Аромат крепкого кофе, в который добавили пару капель коньяка, а так же послевкусие сливочного сыра маскарпоне, десерт так и тает во рту!", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Supernova", name: "Supernova", info: "DarkSide Supernova (Ледяной холод) — хороший дымный табак, который сможет освежить ваши дымные вечера и ваши миксы. Ледяной холод от Darkside это настоящая прохлада без характерного аромата мяты. Прекрасно подходят для разнообразных миксов, идеально освежает без яркого привкуса. Также может куриться соло, для любителей посвежее)", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Nutz", name: "Nutz", info: "DarkSide Nutz (Орех) – насыщенный, очень ароматный табак, наполненный вкусом жареного лесного ореха. Большая длительность времени курения позволяет изысканному вкусу раскрыться в полной мере, обретая нотки шоколада, а количество дыма от этого табака не может не восхитить.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Barvy Orange", name: "Barvy Orange", info: "DarkSide Barvy Orange (Апельсин) — это самый настоящий апельсиновый взрыв. Крепкий, насыщенный вкус апельсина, приятный сладкий цитрусовый аромат. Придется по вкусу абсолютно всем любителям цитрусовых табаков. Отлично миксуется с различными фруктами.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Blackcurrant", name: "Blackcurrant", info: "Darkside Blackcurrant (Черная смородина) — сочный ароматный вкус всеми любимой садовой ягоды. Сладость черносмородинового джема с нежной кислинкой — именно то, что надо, чтобы погреться зимой. Прекрасен сам по себе и хорош в миксах.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Virgin Peach", name: "Virgin Peach", info: "DarkSide Virgin Peach (Персик) — это качественный табак с глубоким, насыщенным вкусом сочного и нежного персика, который сможет порадовать густым, сладким дымом и приятными солнечными ароматами. Он прекрасно курится соло и хорош в сочетании с другими вкусами.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Strawberry Light", name: "Strawberry Light", info: "DarkSide Strawberry Light (Клубника) — это сладкий, приятный кальянный микс со вкусом спелой клубники. Яркий, насыщенный аромат. Рекомендуем.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Generis Raspberry", name: "Generis Raspberry", info: "DarkSide Generis Raspberry (Малина) — наслаждение чистым вкусом садовой малины с легкой кислинкой. Яркий и ароматный, этот базовый табак прекрасно идет в любимые миксы, добавляя летнего настроения.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Sambuka Shot", name: "Sambuka Shot", info: "DarkSide Sambuka Shot (Самбука) — сладкую, густую, согревающую самбуку можно поджечь теперь и в чаше с табаком, чтобы прекрасно провести время в аромате аниса и дымной завесе со вкусом изысканного и опьяняющего напитка. Отлично курится как самостоятельный вкус, но приятен и в сочетании с другими.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "French Macaroon", name: "French Macaroon", info: "Darkside French Macaron (Макарун) — нежный вкус известного французского печенья. Аппетитно-ванильный, слегка бисквитный с легкой миндальной ноткой… Будьте аккуратны — сразу же захочется чашечку кофе и билет на самолет до Парижа.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Blackberry", name: "Blackberry", info: "DarkSide Blackberry (Ежевика) — яркий и насыщенный вкус одной из самых интересных лесных ягод. Кисло-сладкая ежевика прекрасна не только сама по себе, но и очень украшает миксы.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "PepperBlast", name: "PepperBlast", info: "DarkSide Pepperblast (Перцовый взрыв) – это неординарный вкус острого перца. Оставляет незабываемое перченное послевкусие. Взрыв остроты гарантирован! Подойдет для курения в чистом виде, а так же советуем попробовать с другими вкусами.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Mango Lassi", name: "Mango Lassi", info: "DarkSide Mango Lassi (Манго Ласси) — это невероятный сладкий вкус  индийского коктейля на  основе йогурта с добавлением специй, сахара, соли и в нашем с вами случаи тропического фрукта манго. Пожалуй, лучший кальянный вкус манго. Очень насыщенный вкус и аромат. Рекомендуем.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Salbei", name: "Salbei", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Red Tea", name: "Red Tea", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "GingerBlast", name: "GingerBlast", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "BananaPapa", name: "BananaPapa", info: "DarkSide Bananapapa (Банановый папа) — это однозначно лучший банановый вкус из всех табаков для кальяна. Невероятный вкус и аромат.  Топовый вкус Дарксайда.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Blueberryblast", name: "Blueberryblast", info: "DarkSide Rare Blueberryblast (Черничный взрыв) — это настоящий черничный бум. Невероятно яркий и сильный вкус и аромат знаменитой лесной ягоды. Рекомендуем.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Dark Side Cola", name: "Dark Side Cola", info: "DarkSide Cola (Кола) – один из самых популярных ароматов от компании D-Tobacco, входящий в ТОП лучших табачных муасселей, имитирующих вкус колы. Пользователи неизменно оценивают этот продукт как «шикарный», «легкий» и «натуральный». DarkSide Cola отличает гармоничный и мягкий купаж, который позволяет ей образовывать яркие букеты с десятками разных кальянных смесей. Наиболее удачно Cola сочетается с хвойными и ореховыми, малиновыми и вишневыми, мятными и кокосовыми, лимонными и чайными табаками.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Dark Mint", name: "Dark Mint", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Deep blue sea", name: "Deep blue sea", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "PineStar", name: "PineStar", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Raw", name: "Raw", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Dark Side Cookie", name: "Dark Side Cookie", info: "DarkSide Cookie (Печенье) — приятный десертный вкус бананового печенья с кусочками шоколада. Тающее наслаждение для эстетов с ароматом свежей выпечки.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Dark Icecream", name: "Dark Icecream", info: "DarkSide Dark Icecream (Шоколадное мороженое) — это нежный, сладкий вкус шоколадного мороженого. Один из самых популярных вкусов табака для кальяна Дарксайд. Хорош как сам по себе, так и в миксах.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Mary Jane", name: "Mary Jane", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Admiral Acbar Cereal", name: "Admiral Acbar Cereal", info: "DarkSide Admiral Acbar Cereal (Овсяная каша) – сладкий и сливочный вкус настоящей геркулесовой каши. В чистом виде аромат «овсянки» понравится не всем кальянщикам, но во многие миксы Admiral Acbar Cereal добавит необычный акцент. В смесях овсяные хлопья прекрасно сочетаются с ягодами и фруктами, пряностями, десертными и чайными табаками. Особенно приятным послевкусием обладают миксы «овсянки» с малиной, корицей и яблоком.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Applecot", name: "Applecot", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Pear", name: "Pear", info: "DarkSide Pear (Груша) — это превосходный грушевый кальянный вкус, чем-то отдаленно напоминающий лимонад дюшес. Входит в ТОП-10 вкусов Дарксайда.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Kalee Grapefruit", name: "Kalee Grapefruit", info: "DarkSide Kalee Grapefruit (Грейпфрут) – это, безусловно, табак для любителей цитрусовых! Сладкая горечь грейпфрута с яркой кислой основой, удовлетворит даже самых искушенных гурманов.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Torpedo", name: "Torpedo", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Lemonblast", name: "Lemonblast", info: "DarkSide Lemonblast (Лимонный взрыв) – это взрыв вкуса, наполненный яркой цитрусовой кислинкой лимона. Вкус прекрасен как сам по себе, так и в сочетании с другими вкусами.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Virgin Melon", name: "Virgin Melon", info: "DarkSide Virgin Melon (Дыня) — один из наиболее популярных вкусов. Качественный табак с изумительными вкусовыми характеристиками: сладкая, сочная и ароматная дыня в твоем кальяне. Долго курится и отлично смешивается.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Redberry", name: "Redberry", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Wildberry", name: "Wildberry", info: "", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Barvy Citrus", name: "Barvy Citrus", info: "Darkside Barvy Citrus (Цитрусовый микс) — всегда интересный, всегда яркий и всегда насыщенный вкус цитрусовых отличается своим приятным сочетанием сочной сладости и игривой кислинки. Он прекрасно курится без добавления других табаков и интересен в составе различных миксов.", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Spicy Pear", name: "Spicy Pear", info: "Darkside Spicy Pear - Всеми полюбившийся вкус Груши от Дарк Сайда, но уже с добавлением пряностей) однозначно очередной ТОП!", tabaccoID: "Dark Side"),

                    Flavore.init(id: "Barberry Gum", name: "Barberry Gum", info: "DarkSide Barberry gum (Барбарисовая жвачка) — это табак со сладким вкусом барбариски из детства — сладкий, сочный и слегка кислый вкус, который дополняется насыщенным ароматом барбариса, немного похож на вкусы сладких жвачек от других производителей табаков, но имеет неповторимые оттенки и аромат. Интересно раскрывается соло и приятен в смешении с некоторыми другими вкусами.", tabaccoID: "Dark Side")]


        lines = [LineTabacco.init(id: "Soft", name: "Soft"),
                 LineTabacco.init(id: "Medium", name: "Medium"),
                 LineTabacco.init(id: "Rare", name: "Rare")]

        mixes = [
            Mix.init(name: "New mix 1", flavore: [
                MixFlavoreds.init(percent: 30,
                                  tabaccoIDs: "Dark Side",
                                  id: "Eclipse",
                                  name: "Eclipse",
                                  info: "",
                                  linesTabaccoIDs: []),
                MixFlavoreds.init(percent: 20,
                                  tabaccoIDs: "Serbetli",
                                  id: "Barberry Gum",
                                  name: "Barberry Gum",
                                  info: "",
                                  linesTabaccoIDs: []),
                MixFlavoreds.init(percent: 50,
                                  tabaccoIDs: "Tangiers",
                                  id: "Wildberry",
                                  name: "Wildberry",
                                  info: "",
                                  linesTabaccoIDs: [])
                ]),
            Mix.init(name: "New mix 2", flavore: [
                MixFlavoreds.init(percent: 20,
                                  tabaccoIDs: "Ad",
                                  id: "Milk",
                                  name: "Barberry Gum",
                                  info: "",
                                  linesTabaccoIDs: []),
                MixFlavoreds.init(percent: 50,
                                  tabaccoIDs: "El",
                                  id: "Pine",
                                  name: "Pine",
                                  info: "",
                                  linesTabaccoIDs: [])
                ]),
            Mix.init(name: "New mix 3", flavore: [
                MixFlavoreds.init(percent: 30,
                                  tabaccoIDs: "Tangieres",
                                  id: "Kashmir",
                                  name: "Kashmir",
                                  info: "",
                                  linesTabaccoIDs: []),
                MixFlavoreds.init(percent: 20,
                                  tabaccoIDs: "Alf",
                                  id: "Lemon",
                                  name: "Lemon",
                                  info: "",
                                  linesTabaccoIDs: []),
                MixFlavoreds.init(percent: 50,
                                  tabaccoIDs: "Milano",
                                  id: "Road",
                                  name: "Road",
                                  info: "",
                                  linesTabaccoIDs: [])
                ])
        ]
    }
    
    
    func push() {
        
        let jsonEncoder = JSONEncoder()

        countrys.forEach { (countr) in
            do {
                let jsonData = try jsonEncoder.encode(countr)
                let dictFromData: [String: Any] = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String : Any]
                db.collection("country").document(countr.id).setData(dictFromData)
            } catch {
                print(error)
            }
        }
        
        tabaccos.forEach { (tabac) in
            do {
                let jsonData = try jsonEncoder.encode(tabac)
                let dictFromData: [String: Any] = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String : Any]
                db.collection("tabaccos").document(tabac.id).setData(dictFromData)
            } catch {
                print(error)
            }
        }
        
        flavores.forEach { (flav) in
            do {
                let jsonData = try jsonEncoder.encode(flav)
                let dictFromData: [String: Any] = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: Any]
                db.collection("flavores").document(flav.id).setData(dictFromData)
            } catch {
                print(error)
            }
        }
        
        mixes.forEach { (mix) in
            do {
                let jsonData = try jsonEncoder.encode(mix)
                let dictFromData: [String: Any] = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: Any]
                db.collection("mixes").document(mix.name!).setData(dictFromData)
            } catch {
                print(error)
            }
        }
    }
}

