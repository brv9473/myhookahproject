//
//  LineMdel.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/10/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation

struct LineTabacco {
    let id: String
    let name: String
}
