//
//  FlavoreModel.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/10/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation
import Firebase


class Flavore: Codable {
    
    // MARK: - Properties
    var id: String
    var name: String
    var info: String
    var tabaccoID: String
    
    init(id: String, name: String, info: String, tabaccoID : String) {
        self.id = id
        self.name = name
        self.info = info
        self.tabaccoID = tabaccoID
    }
}
