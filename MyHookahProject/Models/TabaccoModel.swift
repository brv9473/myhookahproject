//
//  TabaccoModel.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/10/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation
import Firebase

struct Tabacco: Codable {
    
    // MARK: - Properties
    
    let id: String
    let name: String
    let countryID: String
}




