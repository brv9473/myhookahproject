
//
//  MixFlavoreds.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/18/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation
import Firebase


class MixFlavoreds: Flavore {
    var percent: Int

     enum CodingKeys: String, CodingKey {
        case percent
    }
    
    init(percent: Int, tabaccoIDs: String, id: String, name: String, info: String, linesTabaccoIDs: [String]) {
        self.percent = percent
        super.init(id: id, name: name, info: info, tabaccoID: tabaccoIDs)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.percent = try container.decode(Int.self, forKey: .percent)
        try super.init(from: decoder)
    }
}
