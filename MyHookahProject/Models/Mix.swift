//
//  Mix.swift
//  Hookah
//
//  Created by Rostyslav Bodnar on 7/18/19.
//  Copyright © 2019 Rostyslav Bodnar. All rights reserved.
//

import Foundation
import Firebase

class Mix: Codable {
    
    var name: String?
    var flavore: [MixFlavoreds]
    
    init(name: String?, flavore: [MixFlavoreds]) {
        self.name = name
        self.flavore = flavore
    }
}


